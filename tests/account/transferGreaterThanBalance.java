/* === This file is part of Jive JML dynamic program verifier ===
 *
 *   Copyright 2012, Arvind S Raj <sraj[dot]arvind[at]gmail[dot]com>
 *
 *   Jive JML dynamic program verifier is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU General Public License as
 *   published by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Jive JML Dynamic Program Verifier is distributed in the hope that it
 *   will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *   See the GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Jive JML Dynamic Program Verifier. If not, see
 *   <http://www.gnu.org/licenses/>.
 */

package account;

import java.io.*;

public class transferGreaterThanBalance {
    public static void main(String args[]) {
        Account acc1 = new Account(200);
        Account acc2 = new Account(500);
        acc1.transferAmount(1000, acc2);
        System.out.println("New balance in acc1: " + acc1.getBalance());
        System.out.println("New balance in acc2: " + acc2.getBalance());
    }
};
